package com.cubixsoft.tabletdesign.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.cubixsoft.tabletdesign.R
import com.cubixsoft.tabletdesign.models.ItemsViewModelForExpand

class CommunitypROFILEAdapter(
    private val context: Context,
    private val raceModel: List<ItemsViewModelForExpand>?,
) :
    RecyclerView.Adapter<CommunitypROFILEAdapter.ViewHolder>() {


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val llSHoeHide: LinearLayout = itemView.findViewById(R.id.llSHoeHide)
        val layout: LinearLayout = itemView.findViewById(R.id.mainLayoutExpand)
        val ivUpShow: ImageView = itemView.findViewById(R.id.ivUpShow)
        val ivDownShow: ImageView = itemView.findViewById(R.id.ivDownShow)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val itemView =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.row_community_profile_rv, parent, false)
        return ViewHolder(itemView)
    }

    @SuppressLint("SimpleDateFormat", "SetTextI18n", "ResourceAsColor")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        //
        val model = raceModel?.get(position)
        //        holder.tv_Time.setText(model.getTime());
//        bottomShown(model!!, holder, holder.tv_seemore)

        if (model != null) {

//            holder.image.setBackgroundResource(model.image)
            holder.llSHoeHide.setOnClickListener {
                Toast.makeText(context,"clicked",Toast.LENGTH_SHORT).show()
                if (model.bottomShown === false) {
                    model.bottomShown == true
                    notifyDataSetChanged()
                } else {
                    model.bottomShown == false
                    notifyDataSetChanged()
                }
            }
        }







        holder.itemView.setOnClickListener {
//
//            (context as MainActivity).navController.navigate(R.id.action_homeFragment_to_addCartFragment)
////            Navigation.createNavigateOnClickListener(R.id.action_homeFragment_to_categoryFragment)

        }

    }


    override fun getItemCount() = raceModel!!.size
    private fun bottomShown(
        listItem: ItemsViewModelForExpand,
        holder: ViewHolder,
        tv_seemore: TextView,
    ) {
        if (listItem.bottomShown === true) {
            holder.layout.setVisibility(View.VISIBLE)
            holder.ivDownShow.setVisibility(View.GONE)
            holder.ivUpShow.setVisibility(View.VISIBLE)
            tv_seemore.text = "Close"
            holder.ivDownShow.setBackgroundResource(R.drawable.ic_baseline_keyboard_arrow_up_24)
        } else {
            holder.layout.setVisibility(View.GONE)
            holder.ivUpShow.setVisibility(View.GONE)
            holder.ivDownShow.setVisibility(View.VISIBLE)
            holder.ivDownShow.setBackgroundResource(R.drawable.ic_baseline_keyboard_arrow_down_24)
            tv_seemore.text = "See More"
        }
    }

}
