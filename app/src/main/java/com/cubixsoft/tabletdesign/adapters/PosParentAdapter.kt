package com.cubixsoft.tabletdesign.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cubixsoft.tabletdesign.R
import com.cubixsoft.tabletdesign.models.Child
import com.cubixsoft.tabletdesign.models.Parent


class ParentRecyclerViewAdapter(exampleList: ArrayList<Parent>, context: Context) :
    RecyclerView.Adapter<ParentRecyclerViewAdapter.MyViewHolder>() {
    private val parentModelArrayList: ArrayList<Parent>
    var cxt: Context

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var category: TextView
        var childRecyclerView: RecyclerView

        init {
            category = itemView.findViewById(R.id.title)
            childRecyclerView = itemView.findViewById(R.id.child_recyclerview)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_pos_home, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return parentModelArrayList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val currentItem: Parent = parentModelArrayList[position]
        val layoutManager: RecyclerView.LayoutManager =
            LinearLayoutManager(cxt, LinearLayoutManager.HORIZONTAL, false)
        holder.childRecyclerView.layoutManager = layoutManager
        holder.childRecyclerView.setHasFixedSize(true)
//        holder.category.setText(currentItem.movieCategory())
        val arrayList: ArrayList<Child> = ArrayList()

        // added the first child row
        val childRecyclerViewAdapter =
            PosChildAdapter(arrayList, holder.childRecyclerView.context)
        holder.childRecyclerView.adapter = childRecyclerViewAdapter
    }

    init {
        parentModelArrayList = exampleList
        cxt = context
    }
}