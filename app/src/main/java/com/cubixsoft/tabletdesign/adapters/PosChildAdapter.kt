package com.cubixsoft.tabletdesign.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.cubixsoft.tabletdesign.R
import com.cubixsoft.tabletdesign.models.Child


class PosChildAdapter(arrayList: ArrayList<Child>, var cxt: Context) :
    RecyclerView.Adapter<PosChildAdapter.MyViewHolder>() {
    var childModelArrayList: ArrayList<Child>

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var heroImage: ImageView
        var movieName: TextView

        init {
            heroImage = itemView.findViewById(R.id.profile_image)
            movieName = itemView.findViewById(R.id.title)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_pos_parent, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val currentItem: Child = childModelArrayList[position]
//        holder.heroImage.setImageResource(currentItem.getHeroImage())
        holder.movieName.setText(currentItem.text)
    }

    override fun getItemCount(): Int {
        return childModelArrayList.size
    }

    init {
        childModelArrayList = arrayList
    }
}