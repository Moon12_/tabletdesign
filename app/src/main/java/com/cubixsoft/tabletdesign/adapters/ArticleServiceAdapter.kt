package com.cubixsoft.tabletdesign.adapters
import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.comix.rounded.RoundedCornerImageView
import com.cubixsoft.tabletdesign.R
import com.cubixsoft.tabletdesign.models.ItemsViewModel

class ArticleServiceAdapter(private val context: Context, private val raceModel: List<ItemsViewModel>?) :
    RecyclerView.Adapter<ArticleServiceAdapter.ViewHolder>() {


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val image: RoundedCornerImageView = itemView.findViewById(R.id.imageView1)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.row_article_services, parent, false)
        return ViewHolder(itemView)
    }

    @SuppressLint("SimpleDateFormat", "SetTextI18n", "ResourceAsColor")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        //
        val model = raceModel?.get(position)
        if (model != null) {

            holder.image.setBackgroundResource(model.image)
        }




        holder.itemView.setOnClickListener {
//
//            (context as MainActivity).navController.navigate(R.id.action_homeFragment_to_addCartFragment)
////            Navigation.createNavigateOnClickListener(R.id.action_homeFragment_to_categoryFragment)

        }

    }


    override fun getItemCount() = raceModel!!.size

}
