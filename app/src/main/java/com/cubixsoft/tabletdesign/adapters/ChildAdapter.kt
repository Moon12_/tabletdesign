package com.cubixsoft.tabletdesign.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.cubixsoft.tabletdesign.MainActivity
import com.cubixsoft.tabletdesign.R
import com.cubixsoft.tabletdesign.fragments.ArticleAndServiceFragment
import com.cubixsoft.tabletdesign.models.ChildModel


class ChildAdapter(private val children: List<ChildModel>, val context: Context) :
    RecyclerView.Adapter<ChildAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_pos_parent, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return children.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val child = children[position]
//        holder.imageView.setImageResource(child.image)
        holder.textView.text = child.title
        holder.itemView.setOnClickListener {
//            OptionsFragment optionsFrag = new OptionsFragment ();
//            ((ActivityName)context).getSupportFragmentManager().beginTransaction().replace(R.id.container, optionsFrag,"OptionsFragment").addToBackStack(null).commit();
//        }
//    });
            val myFragment: Fragment = ArticleAndServiceFragment()
            (context as MainActivity).supportFragmentManager.beginTransaction()
                .replace(R.id.fragment, myFragment).addToBackStack(null).commit()

        }
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val textView: TextView = itemView.findViewById(R.id.title)
        val imageView: ImageView = itemView.findViewById(R.id.profile_image)

    }
}