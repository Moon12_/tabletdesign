package com.cubixsoft.tabletdesign.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import com.cubixsoft.tabletdesign.R

class CreateNewServiceActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_create_new_service)
        val ivBack=findViewById<ImageView>(R.id.ivBack)
        ivBack.setOnClickListener {
            finish()
        }
    }
}