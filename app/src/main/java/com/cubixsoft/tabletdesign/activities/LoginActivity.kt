package com.cubixsoft.tabletdesign.activities

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import com.cubixsoft.tabletdesign.MainActivity
import com.cubixsoft.tabletdesign.Utilities
import com.cubixsoft.tabletdesign.base.BaseActivityWithoutVM
import com.cubixsoft.tabletdesign.databinding.ActivityLoginBinding
import com.cubixsoft.tabletdesign.models.apis.UserResponse
import com.cubixsoft.tabletdesign.services.ApiClient
import com.kaopiz.kprogresshud.KProgressHUD
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginActivity : BaseActivityWithoutVM<ActivityLoginBinding>() {
    override fun getViewBinding(): ActivityLoginBinding =
        ActivityLoginBinding.inflate(layoutInflater)

    private lateinit var apiClient: ApiClient
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(mViewBinding.root)
        apiClient = ApiClient()
        mViewBinding.apply {

            onClicks()
        }


    }


    private fun onClicks() {
        mViewBinding.apply {
            ivBack.setOnClickListener {
                finish()
            }
            bLogin.setOnClickListener {
                val intent = Intent(this@LoginActivity, MainActivity::class.java)
                startActivity(intent)
                val getEmail: String = etEmail.text.toString()
                val getPass: String = etpassword.text.toString()
//                if (!getEmail.equals("")) {
//                    if (!getPass.equals("")) {
//                        loginApi(getEmail, getPass)
//                    } else {
//                        showToast("Enter Password")
//                    }
//                } else {
//                    showToast("Enter Email Address")
//
//                }
            }
        }
    }


    fun loginApi(email: String, pass: String) {
        val hud = KProgressHUD.create(this)
            .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
            .setLabel("Please wait")
            .setCancellable(true)
            .setAnimationSpeed(2)
            .setDimAmount(0.5f)
            .show()

        apiClient.getApiService().login(email, pass)
            .enqueue(object : Callback<UserResponse> {
                override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                    hud.dismiss()
                    Toast.makeText(applicationContext, "false" + t.message, Toast.LENGTH_SHORT)
                        .show()
                }

                override fun onResponse(
                    call: Call<UserResponse>,
                    response: Response<UserResponse>,
                ) {
                    val loginResponse = response.body()

                    if (response.isSuccessful) {
                        hud.dismiss()
                        showToast("Login Successfuly")
                        val intent = Intent(this@LoginActivity, MainActivity::class.java)
                        Utilities.saveString(
                            this@LoginActivity,
                            Utilities.AccessToken,
                            loginResponse?.accessToken
                        )
                        Utilities.saveString(this@LoginActivity, "loginStatus", "yes")
                        Utilities.saveString(this@LoginActivity,
                            Utilities.reFreshToken,
                            loginResponse?.refreshToken
                        )

                        startActivity(intent)
                        finish()


                    } else {
//                        val loginResponse = response.body()
                        hud.dismiss()
                        if (loginResponse != null) {
                            showToast("response not successfull")
                        }
                    }
                }
            })
    }
}