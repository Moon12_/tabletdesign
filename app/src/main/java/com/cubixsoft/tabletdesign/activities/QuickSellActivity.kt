package com.cubixsoft.tabletdesign.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import com.cubixsoft.tabletdesign.R

class QuickSellActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_quick_sell)
        val ivBack=findViewById<ImageView>(R.id.ivBack)
        ivBack.setOnClickListener {
            finish()
        }
    }
}