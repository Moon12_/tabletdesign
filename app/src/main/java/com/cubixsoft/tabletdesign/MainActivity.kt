package com.cubixsoft.tabletdesign

import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.navigation.NavController
import com.cubixsoft.tabletdesign.base.BaseActivityWithoutVM
import com.cubixsoft.tabletdesign.databinding.ActivityMainBinding
import com.cubixsoft.tabletdesign.fragments.CommunityFragment
import com.cubixsoft.tabletdesign.fragments.HomeFragment
import com.cubixsoft.tabletdesign.fragments.InventoryFragment
import com.cubixsoft.tabletdesign.fragments.LocationFragment
import com.google.android.material.navigationrail.NavigationRailView
import libs.mjn.prettydialog.PrettyDialog
import libs.mjn.prettydialog.PrettyDialogCallback

class MainActivity : BaseActivityWithoutVM<ActivityMainBinding>() {
     lateinit var navController: NavController

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
    @SuppressLint("UseCompatLoadingForDrawables")
    override fun getViewBinding(): ActivityMainBinding =
        ActivityMainBinding.inflate(layoutInflater)
    @SuppressLint("ResourceAsColor")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(mViewBinding.root)

//
        val navigationRailView = findViewById<NavigationRailView>(R.id.navigation_rail)
        val llQuickSell = findViewById<LinearLayout>(R.id.llQuickSell)
        val llTop = findViewById<LinearLayout>(R.id.llTop)
        val createService = findViewById<LinearLayout>(R.id.createService)
        navigationRailView.itemIconTintList = null
        llTop.visibility = View.VISIBLE

        mViewBinding.apply {
            llLocation.setOnClickListener {
                llLocation.setBackgroundResource(R.drawable.gradient_shape)
                llpos.setBackgroundResource(R.drawable.tranparent_shape)
                llCommunity.setBackgroundResource(R.drawable.tranparent_shape)
                llinventory.setBackgroundResource(R.drawable.tranparent_shape)
                if (savedInstanceState == null) {
                    // 2
                    supportFragmentManager
                        // 3
                        .beginTransaction()
                        // 4
                        .add(R.id.fragment, LocationFragment.newInstance(), "dogList")
                        // 5
                        .commit()
                }
            }
            llpos.setOnClickListener {
                llLocation.setBackgroundResource(R.drawable.tranparent_shape)
                llpos.setBackgroundResource(R.drawable.gradient_shape)
                llCommunity.setBackgroundResource(R.drawable.tranparent_shape)
                llinventory.setBackgroundResource(R.drawable.tranparent_shape)
                if (savedInstanceState == null) {
                    // 2
                    supportFragmentManager
                        // 3
                        .beginTransaction()
                        // 4
                        .add(R.id.fragment, HomeFragment.newInstance(), "dogList")
                        // 5
                        .commit()
                }
            }
            llCommunity.setOnClickListener {
                llLocation.setBackgroundResource(R.drawable.tranparent_shape)
                llpos.setBackgroundResource(R.drawable.tranparent_shape)
                llCommunity.setBackgroundResource(R.drawable.gradient_shape)
                llinventory.setBackgroundResource(R.drawable.tranparent_shape)
                if (savedInstanceState == null) {
                    // 2
                    supportFragmentManager
                        // 3
                        .beginTransaction()
                        // 4
                        .add(R.id.fragment, CommunityFragment.newInstance(), "dogList")
                        // 5
                        .commit()
                }
            }
            llinventory.setOnClickListener {
                llLocation.setBackgroundResource(R.drawable.tranparent_shape)
                llpos.setBackgroundResource(R.drawable.tranparent_shape)
                llCommunity.setBackgroundResource(R.drawable.tranparent_shape)
                llinventory.setBackgroundResource(R.drawable.gradient_shape)
                if (savedInstanceState == null) {
                    // 2
                    supportFragmentManager
                        // 3
                        .beginTransaction()
                        // 4
                        .add(R.id.fragment, InventoryFragment.newInstance(), "dogList")
                        // 5
                        .commit()
                }
            }

        }

        navigationRailView.setOnItemSelectedListener { menuItem ->
            when (menuItem.itemId) {
                R.id.threeSixty -> {
                    Toast.makeText(this, "pos", Toast.LENGTH_SHORT).show()
                    if (savedInstanceState == null) {
                        // 2
                        supportFragmentManager
                            // 3
                            .beginTransaction()
                            // 4
                            .add(R.id.fragment, LocationFragment.newInstance(), "dogList")
                            // 5
                            .commit()
                    }
                    true
                }
                R.id.posNav -> {
                    if (savedInstanceState == null) {
                        // 2
                        supportFragmentManager
                            // 3
                            .beginTransaction()
                            // 4
                            .add(R.id.fragment, HomeFragment.newInstance(), "dogList")
                            // 5
                            .commit()
                    }
                    true
                }
                R.id.connectNav -> {
                    if (savedInstanceState == null) {
                        // 2
                        supportFragmentManager
                            // 3
                            .beginTransaction()
                            // 4
                            .add(R.id.fragment, CommunityFragment.newInstance(), "dogList")
                            // 5
                            .commit()
                    }
                    true
                }
                R.id.inventoryTab -> {
                    if (savedInstanceState == null) {
                        // 2
                        supportFragmentManager
                            // 3
                            .beginTransaction()
                            // 4
                            .add(R.id.fragment, InventoryFragment.newInstance(), "dogList")
                            // 5
                            .commit()
                    }
                    true
                }
                else -> false
            }
        }
    }

    private fun showCustomDialogforExit() {
        val pDialog = PrettyDialog(this)
        pDialog
            .setTitle("Message")
            .setMessage("Are you sure you want to Exit?")
            .setIcon(R.drawable.pdlg_icon_info)
            .setIconTint(R.color.colorPrimary)
            .addButton(
                "Yes",
                R.color.colorPrimary,
                R.color.pdlg_color_white,
                object : PrettyDialogCallback {
                    override fun onClick() {
                        pDialog.dismiss()
                        finish()
                        finishAffinity()
                    }
                })
            .addButton("No",
                R.color.pdlg_color_red,
                R.color.pdlg_color_white,
                object : PrettyDialogCallback {
                    override fun onClick() {
                        pDialog.dismiss()
                    }
                })
            .show()
    }

    override fun onBackPressed() {

            super.onBackPressed()

    }

    private fun showCustomDialog() {
        val pDialog = PrettyDialog(this)
        pDialog
            .setTitle("Message")
            .setMessage("Are you sure you want to Logout?")
            .setIcon(R.drawable.pdlg_icon_info)
            .setIconTint(R.color.color_primary)
            .addButton(
                "Yes",
                R.color.pdlg_color_white,
                R.color.color_primary
            ) {
                Utilities.clearSharedPref(this)
//                startActivity(Intent(this, SplashActivity::class.java))
//                finish()
                pDialog.dismiss()
            }
            .addButton(
                "No",
                R.color.pdlg_color_white,
                R.color.pdlg_color_red
            ) { pDialog.dismiss() }
            .show()
    }

}