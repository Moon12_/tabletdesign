package com.cubixsoft.tabletdesign.models.apis

import com.google.gson.annotations.SerializedName

data class UserResponse(
    @SerializedName("accessToken")
    var accessToken: String,
    @SerializedName("refreshToken")
    var refreshToken: String,
    @SerializedName("idToken")
    var idToken: String,
)





