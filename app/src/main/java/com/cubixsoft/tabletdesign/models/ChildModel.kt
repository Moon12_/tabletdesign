package com.cubixsoft.tabletdesign.models

data class ChildModel(
        val image : Int = -1,
        val title : String = ""
)