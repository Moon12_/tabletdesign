package com.cubixsoft.tabletdesign.models;

public class ExpandableModel {


    String id;
    String time;
    private boolean bottomShown;

    public ExpandableModel(boolean bottomShown) {
        this.bottomShown = bottomShown;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public boolean isBottomShown() {
        return bottomShown;
    }

    public void setBottomShown(boolean bottomShown) {
        this.bottomShown = bottomShown;
    }
}
