package com.cubixsoft.tabletdesign.models

data class ParentModel (
        val title : String = "",
        val children : List<ChildModel>
)