package com.cubixsoft.tabletdesign.services

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient


class ApiClient {
    private lateinit var apiService: ApiService
    var gson = GsonBuilder()
        .setLenient()
        .create()
    private val client = OkHttpClient.Builder().build()

    fun getApiService(): ApiService {

        // Initialize ApiService if not initialized yet
        if (!::apiService.isInitialized) {
            val retrofit = Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()

            apiService = retrofit.create(ApiService::class.java)
        }

        return apiService
    }

}