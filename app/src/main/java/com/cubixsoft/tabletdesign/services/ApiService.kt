package com.cubixsoft.tabletdesign.services


import com.cubixsoft.tabletdesign.models.apis.UserResponse
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST


interface ApiService {
    @POST(Constants.LOGIN_URL)
    @FormUrlEncoded
    fun login(
        @Field("email") email: String?,
        @Field("password") password: String?,
    ):
            Call<UserResponse>
//
//    @POST("forgotPassword")
//    @FormUrlEncoded
//    fun forgotPasswordApi(
//        @Field("email") email: String?,
//    ):
//            Call<ForgotPassResponse>
//
//    @POST("signUp")
//    @FormUrlEncoded
//    fun signUp(
//        @Field("first_name") first_name: String?,
//        @Field("last_name") last_name: String?,
//        @Field("email") email: String?,
//        @Field("password") password: String?,
//        @Field("fcm_token") token: String?,
//    ):
//            Call<CommonResponse>
//
//    @POST("changePassword")
//    @FormUrlEncoded
//    fun changePassword(
//        @Field("email") email: String?,
//        @Field("new_password") password: String?,
//    ):
//            Call<CommonResponse>
//
//
//
//    @POST("updateProfile")
//    @FormUrlEncoded
//    fun updateProfileApi(
//        @Field("user_id") user_id: String?,
//        @Field("first_name") first_name: String?,
//        @Field("last_name") last_name: String?,
//        @Field("email") email: String?,
//    ):
//            Call<UserResponse>
//
//    @POST("updatePassword")
//    @FormUrlEncoded
//    fun changePassApi(
//        @Field("user_id") user_id: String?,
//        @Field("old_password") old_password: String?,
//        @Field("new_password") new_password: String?,
//    ):
//            Call<UserResponse>
//
//
//    @POST("checkIn")
//    @FormUrlEncoded
//    fun checkIn(
//        @Field("previous_weight") previous_weight: String?,
//        @Field("current_weight") current_weight: String?,
//        @Field("comment") comment: String?,
//        @Field("image_1") image_1: String?,
//        @Field("image_2") image_2: String?,
//        @Field("image_3") image_3: String?,
//        @Field("image_4") image_4: String?,
//        @Field("user_id") user_id: String?,
//    ):
//            Call<CheckinResponse>
//
//
//    @POST("addImage")
//    @FormUrlEncoded
//    fun addImage(
//        @Field("previous_weight") previous_weight: String?,
//        @Field("current_weight") current_weight: String?,
//        @Field("image") image: String?,
//        @Field("user_id") user_id: String?,
//    ):
//            Call<CommonResponse>
//
//
//    @POST("getImages")
//    @FormUrlEncoded
//    fun getImages(
//        @Field("user_id") user_id: String?,
//        @Field("month") month: String?,
//    ):
//            Call<MainGetImageResponse>
//
//    @POST("sub_category")
//    fun getSubCategoryApi(
//        @Query("id") id: Int):
//            Call<MainSubCategResponse>
//
//    @POST("artical")
//    fun gegDetailArticelsApi(
//        @Query("id") id: Int?
//    ):
//            Call<MainAtricleDetailResponse>
//
//
//    @POST("products_details")
//    fun getProductDetailApi(
//        @Query("id") id: Int?
//    ):
//            Call<MainProductDetailResponse>
//
//    @POST("products")
//    fun getProductApi(
//        @Query("id") id: Int?
//    ):
//            Call<MainProductsResponse>
//
//    @GET("categories")
//    fun getCategory():
//            Call<MainCategResponse>
//
//
//    @GET("artical_catogery")
//    fun getArticalCategory():
//            Call<MainArticlesCategResponse>
//
//    @GET("tips")
//    fun getTips():
//            Call<MainTipsResponse>
}