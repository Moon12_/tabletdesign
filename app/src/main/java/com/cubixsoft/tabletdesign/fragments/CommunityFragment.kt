package com.cubixsoft.tabletdesign.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cubixsoft.tabletdesign.R
import com.cubixsoft.tabletdesign.adapters.CommunityActivitiesAdapter
import com.cubixsoft.tabletdesign.adapters.CommunityAdapter
import com.cubixsoft.tabletdesign.adapters.CommunityCheckinAdapter
import com.cubixsoft.tabletdesign.adapters.ExpandAbleAdapter
import com.cubixsoft.tabletdesign.databinding.FragmentCommunityBinding
import com.cubixsoft.tabletdesign.models.ExpandableModel
import com.cubixsoft.tabletdesign.models.ItemsViewModel
import com.google.android.material.tabs.TabLayout
import java.util.*

class CommunityFragment : Fragment() {
    private lateinit var binding: FragmentCommunityBinding
    var list_of_items = arrayOf("Item 1", "Item 2", "Item 3")
    lateinit var recyclerView: RecyclerView

    companion object {

        fun newInstance(): CommunityFragment {
            return CommunityFragment()
        }
    }

    @SuppressLint("UseRequireInsteadOfGet", "ResourceAsColor")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        binding = FragmentCommunityBinding.inflate(inflater, container, false)
        binding.setting.setOnClickListener {
            activity!!.supportFragmentManager
                // 3
                .beginTransaction()
                // 4
                .add(R.id.fragment, ArticleAndServiceFragment.newInstance(), "dogList")
                // 5
                .commit()

        }
        setManagmentData()
        setCheckInData()
        setTab()
        return binding.root
    }

    fun setManagmentData() {
        binding.rvManagemnt.layoutManager =
            GridLayoutManager(activity, 1, LinearLayoutManager.VERTICAL, false)

        // ArrayList of class ItemsViewModel
        val data = ArrayList<ItemsViewModel>()

        // This loop will create 20 Views containing
        // the image with the count of view
        data.add(ItemsViewModel(R.drawable.demo_baba, "Beverage "))
        data.add(ItemsViewModel(R.drawable.demo_baba, "Fresh Food "))
        data.add(ItemsViewModel(R.drawable.demo_baba, "Beverage "))
        data.add(ItemsViewModel(R.drawable.demo_baba, "Fresh Food "))
        data.add(ItemsViewModel(R.drawable.demo_baba, "Beverage "))
        data.add(ItemsViewModel(R.drawable.demo_baba, "Fresh Food "))
        data.add(ItemsViewModel(R.drawable.demo_baba, "Beverage "))
        data.add(ItemsViewModel(R.drawable.demo_baba, "Fresh Food "))

        // This will pass the ArrayList to our Adapter
        val adapter = CommunityAdapter(requireActivity(), data)
        // Setting the Adapter with the recyclerview
        binding.rvManagemnt.adapter = adapter
    }

    fun setCheckInData() {
        binding.rvCheckInTab.visibility = View.VISIBLE
        binding.rvCheckInTab.layoutManager =
            GridLayoutManager(activity, 2, LinearLayoutManager.VERTICAL, false)

        // ArrayList of class ItemsViewModel
        val data = ArrayList<ItemsViewModel>()
        for (i in 0..3) {
            data.add(ItemsViewModel(R.drawable.demo_baba, "Beverage "))

        }
        // This loop will create 20 Views containing
        // the image with the count of view
//        data.add(ItemsViewModel(R.drawable.demo_baba, "Beverage "))
//        data.add(ItemsViewModel(R.drawable.demo_baba, "Fresh Food "))
//        data.add(ItemsViewModel(R.drawable.demo_baba, "Beverage "))
//        data.add(ItemsViewModel(R.drawable.demo_baba, "Fresh Food "))
//        data.add(ItemsViewModel(R.drawable.demo_baba, "Beverage "))
//        data.add(ItemsViewModel(R.drawable.demo_baba, "Fresh Food "))
//        data.add(ItemsViewModel(R.drawable.demo_baba, "Beverage "))
//        data.add(ItemsViewModel(R.drawable.demo_baba, "Fresh Food "))

        // This will pass the ArrayList to our Adapter
        val adapter = CommunityCheckinAdapter(requireActivity(), data)
        // Setting the Adapter with the recyclerview
        binding.rvCheckInTab.adapter = adapter
    }

    fun setCheckInProfileData() {
        binding.rvCheckInTab.visibility = View.VISIBLE

        binding.rvCheckInTab.layoutManager =
            GridLayoutManager(activity, 1, LinearLayoutManager.VERTICAL, false)

        // ArrayList of class ItemsViewModel
        val data = ArrayList<ExpandableModel>()
        for (i in 0..3) {
            data.add(ExpandableModel(false))

        }
        val adapter = ExpandAbleAdapter(requireActivity(), data)
        // Setting the Adapter with the recyclerview
        binding.rvCheckInTab.adapter = adapter
    }

    fun setCheckInActivitiesData() {
        binding.rvCheckInTab.visibility = View.VISIBLE

        binding.rvCheckInTab.layoutManager =
            GridLayoutManager(activity, 2, LinearLayoutManager.VERTICAL, false)

        // ArrayList of class ItemsViewModel
        val data = ArrayList<ItemsViewModel>()
        for (i in 0..3) {
            data.add(ItemsViewModel(R.drawable.demo_baba, "Fresh Food "))


        }
        val adapter = CommunityActivitiesAdapter(requireActivity(), data)
        // Setting the Adapter with the recyclerview
        binding.rvCheckInTab.adapter = adapter
    }

    private fun setTab() {
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText("Check-in")
        )
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText("Activities")
        )
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText("Profile")
        )


        binding.tabLayout.setOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                var fragment: Fragment? = null
                when (tab.position) {
                    //Check-in
                    0 -> {
                        setCheckInData()
                    }
                    //Activities
                    1 -> {
                        setCheckInActivitiesData()                    }
//                    Profile
                    2 -> {
                        setCheckInProfileData()
                    }

                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {}
            override fun onTabReselected(tab: TabLayout.Tab) {}
        })

    }

}