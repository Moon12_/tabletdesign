package com.cubixsoft.tabletdesign.fragments

import android.annotation.SuppressLint
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.RecyclerView
import com.cubixsoft.tabletdesign.R
import com.cubixsoft.tabletdesign.databinding.FragmentCreateNewServiceBinding

class CreateNewServiceFragment : DialogFragment() {
    private lateinit var binding: FragmentCreateNewServiceBinding
    var list_of_items = arrayOf("Item 1", "Item 2", "Item 3")
    lateinit var recyclerView: RecyclerView

    companion object {

        fun newInstance(): CreateNewServiceFragment {
            return CreateNewServiceFragment()
        }
    }

    @SuppressLint("UseRequireInsteadOfGet", "ResourceAsColor")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        binding = FragmentCreateNewServiceBinding.inflate(inflater, container, false)
        if (dialog != null && dialog!!.window != null) {
            dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog!!.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        }

        setupSpinnerForInProgress("Please Select", binding.spinnerNodeProfile)
        setupSpinnerForInProgress("Please Select", binding.spinnerLocations)
        binding.ivBack.setOnClickListener {
            dialog?.dismiss()
        }

        return binding.root
    }




    private fun setupSpinnerForInProgress(nameFirst: String, spinner: Spinner) {
        val personNames =
            arrayOf(nameFirst, "Jack", "Rajeev", "Aryan", "Rashmi", "Jaspreet", "Akbar")
        val spinner = spinner
        val arrayAdapter =
            ArrayAdapter(requireActivity(), android.R.layout.simple_spinner_item, personNames)
        spinner.adapter = arrayAdapter

        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long,
            ) {
                Toast.makeText(
                    activity,
                    getString(R.string.selected_item) + " " + personNames[position],
                    Toast.LENGTH_SHORT
                ).show()
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // Code to perform some action when nothing is selected
            }
        }
    }
}