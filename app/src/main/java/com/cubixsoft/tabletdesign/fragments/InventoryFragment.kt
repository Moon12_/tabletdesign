package com.cubixsoft.tabletdesign.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cubixsoft.tabletdesign.R
import com.cubixsoft.tabletdesign.adapters.CommunityActivitiesAdapter
import com.cubixsoft.tabletdesign.adapters.CommunityCheckinAdapter
import com.cubixsoft.tabletdesign.adapters.ExpandAbleAdapter
import com.cubixsoft.tabletdesign.adapters.InventoryAdapter
import com.cubixsoft.tabletdesign.databinding.FragmentCommunityBinding
import com.cubixsoft.tabletdesign.databinding.FragmentInventoryBinding
import com.cubixsoft.tabletdesign.models.ExpandableModel
import com.cubixsoft.tabletdesign.models.ItemsViewModel
import com.google.android.material.tabs.TabLayout
import java.util.*

class InventoryFragment : Fragment() {
    private lateinit var binding: FragmentInventoryBinding
    var list_of_items = arrayOf("Item 1", "Item 2", "Item 3")
    lateinit var recyclerView: RecyclerView

    companion object {

        fun newInstance(): InventoryFragment {
            return InventoryFragment()
        }
    }

    @SuppressLint("UseRequireInsteadOfGet", "ResourceAsColor")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        binding = FragmentInventoryBinding.inflate(inflater, container, false)
        binding.setting.setOnClickListener {
//            activity!!.supportFragmentManager
//                // 3
//                .beginTransaction()
//                // 4
//                .add(R.id.fragment, ArticleAndServiceFragment.newInstance(), "dogList")
//                // 5
//                .commit()

        }
        setInventoryData()
        return binding.root
    }

    fun setInventoryData() {
        binding.rvManagemnt.layoutManager =
            GridLayoutManager(activity, 1, LinearLayoutManager.VERTICAL, false)

        // ArrayList of class ItemsViewModel
        val data = ArrayList<ItemsViewModel>()
        for (i in 0..5) {
            data.add(ItemsViewModel(R.drawable.demo_inventory, "Beverage "))

        }

        // This will pass the ArrayList to our Adapter
        val adapter = InventoryAdapter(requireActivity(), data)
        // Setting the Adapter with the recyclerview
        binding.rvManagemnt.adapter = adapter
    }


}