package com.cubixsoft.tabletdesign.fragments

import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Bundle
import android.text.format.DateFormat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.cubixsoft.tabletdesign.R
import com.cubixsoft.tabletdesign.adapters.*
import com.cubixsoft.tabletdesign.databinding.FragmentLocationBinding
import com.cubixsoft.tabletdesign.models.ItemsViewModel
import devs.mulham.horizontalcalendar.HorizontalCalendar
import devs.mulham.horizontalcalendar.utils.HorizontalCalendarListener
import java.util.*
class LocationFragment : Fragment() {
    private lateinit var binding: FragmentLocationBinding
    var list_of_items = arrayOf("Item 1", "Item 2", "Item 3")
    lateinit var horizontalCalendar: HorizontalCalendar
    companion object {

        fun newInstance(): LocationFragment {
            return LocationFragment()
        }
    }

    @SuppressLint("UseRequireInsteadOfGet", "ResourceAsColor")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        binding = FragmentLocationBinding.inflate(inflater, container, false)
        binding.llLiveTab.setBackgroundResource(R.drawable.gradient_shape)
        binding.ivIcON.setBackgroundResource(R.drawable.live_ic)
//        createTabIcons()
//        setTab()
        setClicks()
        initRvData()
        val endDate = Calendar.getInstance()
        endDate.add(Calendar.MONTH, 1)

        /** start before 1 month from now */
        /** start before 1 month from now  */
        val startDate = Calendar.getInstance()
        startDate.add(Calendar.MONTH, -1)

        horizontalCalendar = HorizontalCalendar.Builder(binding.root, R.id.calendarView)
            .range(startDate, endDate)
            .datesNumberOnScreen(12)
            .configure()
            .formatTopText("MMM")
            .formatMiddleText("dd")
            .formatBottomText("EEE")
            .showTopText(true)
            .showBottomText(true)
            .textColor(Color.BLACK, Color.BLACK)
            .colorTextMiddle(Color.BLUE, Color.parseColor("#ffd54f"))
            .end()
            .build()

        horizontalCalendar.setCalendarListener(object : HorizontalCalendarListener() {
            override fun onDateSelected(date: Calendar?, position: Int) {
                val selectedDateStr: String = DateFormat.format("yyyy-MM", date).toString()
                val daysOfMonth: String = DateFormat.format("dd", date).toString()
                var finalinte: Int = daysOfMonth.toInt() + 1
//                datefFinal = selectedDateStr + "-" + finalinte
//                showToast(datefFinal)
                Log.i("onDateSelected", "$date - Position = $position")
            }
        })
        return binding.root
    }


    private fun setClicks() {
        binding.llLiveTab.setOnClickListener {
            binding.llLiveTab.setBackgroundResource(R.drawable.gradient_shape)
            binding.llCalendarTab.setBackgroundResource(R.drawable.tranparent_shape)
            binding.llRequestTab.setBackgroundResource(R.drawable.tranparent_shape)
            binding.llStuffTab.setBackgroundResource(R.drawable.tranparent_shape)
            setManagmentData()

        }
        binding.llRequestTab.setOnClickListener {
            binding.llRequestTab.setBackgroundResource(R.drawable.gradient_shape)
            binding.llCalendarTab.setBackgroundResource(R.drawable.tranparent_shape)
            binding.llLiveTab.setBackgroundResource(R.drawable.tranparent_shape)
            binding.llStuffTab.setBackgroundResource(R.drawable.tranparent_shape)
            setRequestTabData()

        }
        binding.llStuffTab.setOnClickListener {
            binding.llStuffTab.setBackgroundResource(R.drawable.gradient_shape)
            binding.llCalendarTab.setBackgroundResource(R.drawable.tranparent_shape)
            binding.llRequestTab.setBackgroundResource(R.drawable.tranparent_shape)
            binding.llLiveTab.setBackgroundResource(R.drawable.tranparent_shape)
            setStuffData()

        }

        binding.llCalendarTab.setOnClickListener {
            binding.llStuffTab.setBackgroundResource(R.drawable.tranparent_shape)
            binding.llCalendarTab.setBackgroundResource(R.drawable.gradient_shape)
            binding.llRequestTab.setBackgroundResource(R.drawable.tranparent_shape)
            binding.llLiveTab.setBackgroundResource(R.drawable.tranparent_shape)
            setCalendarData()

        }
        binding.llQuickSell.setOnClickListener {
            showDialogForQuickSell()
//
        }

        binding.createService.setOnClickListener {
            showDialog()
        }
    }

    private fun initRvData() {
        binding.rvDealOfDay.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        binding.llCalandaer.visibility = View.GONE

        // ArrayList of class ItemsViewModel
        val data = ArrayList<ItemsViewModel>()

        // This loop will create 20 Views containing
        // the image with the count of view
        data.add(ItemsViewModel(R.drawable.demo_user1, "Beverage "))
        data.add(ItemsViewModel(R.drawable.demo_user1, "Fresh Food "))
        data.add(ItemsViewModel(R.drawable.demo_user1, "Beverage "))
        data.add(ItemsViewModel(R.drawable.demo_user1, "Fresh Food "))
        data.add(ItemsViewModel(R.drawable.demo_user1, "Beverage "))


        // This will pass the ArrayList to our Adapter
        val adapter = InYourLocationAdapter(requireActivity(), data)

        // Setting the Adapter with the recyclerview
        binding.rvDealOfDay.adapter = adapter

        binding.rvInternalNotifications.layoutManager =
            GridLayoutManager(activity, 2, LinearLayoutManager.VERTICAL, false)

        // ArrayList of class ItemsViewModel
        val data1 = ArrayList<ItemsViewModel>()

        // This loop will create 20 Views containing
        // the image with the count of view
        data1.add(ItemsViewModel(R.drawable.internal_notification, "Bernard Stainely "))
        data1.add(ItemsViewModel(R.drawable.internal_notification, "445-433-444"))

        data1.add(ItemsViewModel(R.drawable.internal_notification, "Bernard Stainely "))
        data1.add(ItemsViewModel(R.drawable.internal_notification, "Bernard Stainely"))
        data1.add(ItemsViewModel(R.drawable.internal_notification, "445-433-444"))

        data1.add(ItemsViewModel(R.drawable.internal_notification, "445-433-444"))


        // This will pass the ArrayList to our Adapter
        val adapter1 = InternalNotificationAdapter(requireActivity(), data1)
        setManagmentData()
        // Setting the Adapter with the recyclerview
        binding.rvInternalNotifications.adapter = adapter1
    }

    fun setManagmentData() {
        binding.spinnerTodo.visibility = View.GONE
        binding.llSpinner.visibility = View.VISIBLE
        binding.llRecyclerview.visibility = View.VISIBLE
        binding.tvTitle.text = "Management"
        binding.ivIcON.setBackgroundResource(R.drawable.live_ic)
        binding.llCalandaer.visibility = View.GONE

        binding.rvManagemnt.layoutManager =
            GridLayoutManager(activity, 1, LinearLayoutManager.VERTICAL, false)

        // ArrayList of class ItemsViewModel
        val data = ArrayList<ItemsViewModel>()

        // This loop will create 20 Views containing
        // the image with the count of view
        data.add(ItemsViewModel(R.drawable.menu_primary, "Beverage "))
        data.add(ItemsViewModel(R.drawable.menu_primary, "Fresh Food "))
        data.add(ItemsViewModel(R.drawable.menu_primary, "Beverage "))
        data.add(ItemsViewModel(R.drawable.menu_primary, "Fresh Food "))
        data.add(ItemsViewModel(R.drawable.menu_primary, "Beverage "))
        data.add(ItemsViewModel(R.drawable.menu_primary, "Fresh Food "))
        data.add(ItemsViewModel(R.drawable.menu_primary, "Beverage "))
        data.add(ItemsViewModel(R.drawable.menu_primary, "Fresh Food "))

        // This will pass the ArrayList to our Adapter
        val adapter = ManagmentAdapter(requireActivity(), data)
        binding.spinnerInProgress.visibility = View.VISIBLE
        binding.spinnerAllLocation.visibility = View.VISIBLE
        setupSpinnerForInProgress("All Location", binding.spinnerAllLocation)
        setupSpinnerForInProgress("In Progress", binding.spinnerInProgress)
        // Setting the Adapter with the recyclerview
        binding.rvManagemnt.adapter = adapter
    }

    fun setRequestTabData() {
        binding.tvTitle.text = "Assigned to you"
        binding.ivIcON.setBackgroundResource(R.drawable.cap_ic)
        binding.llSpinner.visibility = View.GONE
        binding.llCalandaer.visibility = View.GONE
        binding.llRecyclerview.visibility = View.VISIBLE

        binding.spinnerTodo.visibility = View.VISIBLE
        setupSpinnerForInProgress("Todo", binding.spinnerTodo)
        binding.rvManagemnt.layoutManager =
            GridLayoutManager(activity, 1, LinearLayoutManager.VERTICAL, false)

        // ArrayList of class ItemsViewModel
        val data = ArrayList<ItemsViewModel>()

        // This loop will create 20 Views containing
        // the image with the count of view
        for (i in 0..3) {
            data.add(ItemsViewModel(R.drawable.demo_baba, "Beverage "))

        }
        val adapter = LocationRequestAdapter(requireActivity(), data)
        binding.rvManagemnt.adapter = adapter
    }

    fun setCalendarData() {
        binding.tvTitle.text = "Planning"
        binding.ivIcON.setBackgroundResource(R.drawable.calendar_ic)
        binding.llSpinner.visibility = View.GONE
        binding.llRecyclerview.visibility = View.GONE

        binding.spinnerTodo.visibility = View.VISIBLE
        binding.llCalandaer.visibility = View.VISIBLE
        setupSpinnerForInProgress("Location", binding.spinnerTodo)
        binding.rvCalendar.layoutManager =
            GridLayoutManager(activity, 1, LinearLayoutManager.VERTICAL, false)

        // ArrayList of class ItemsViewModel
        val data = ArrayList<ItemsViewModel>()

        // This loop will create 20 Views containing
        // the image with the count of view
        for (i in 0..7) {
            data.add(ItemsViewModel(R.drawable.demo_baba, "Beverage "))

        }
        val adapter = CalaendarAdapter(requireActivity(), data)
        binding.rvCalendar.adapter = adapter
    }

    fun setStuffData() {
        binding.tvTitle.text = "General Stuff"
        binding.ivIcON.setBackgroundResource(R.drawable.genral_stuff)
        binding.llRecyclerview.visibility = View.VISIBLE

        binding.rvManagemnt.layoutManager =
            GridLayoutManager(activity, 3, LinearLayoutManager.VERTICAL, false)

        // ArrayList of class ItemsViewModel
        val data = ArrayList<ItemsViewModel>()

        // This loop will create 20 Views containing
        // the image with the count of view
        data.add(ItemsViewModel(R.drawable.panda, "Beverage "))
        data.add(ItemsViewModel(R.drawable.panda, "Beverage "))
        data.add(ItemsViewModel(R.drawable.panda, "Beverage "))
        data.add(ItemsViewModel(R.drawable.panda, "Beverage "))

        // This will pass the ArrayList to our Adapter
        val adapter = StaffAdapter(requireActivity(), data)
        setupSpinnerForInProgress("Active", binding.spinnerAllLocation)
        binding.spinnerInProgress.visibility = View.GONE
        // Setting the Adapter with the recyclerview
        binding.rvManagemnt.adapter = adapter
    }


    private fun setupSpinnerForInProgress(nameFirst: String, spinner: Spinner) {
        val personNames =
            arrayOf(nameFirst, "Jack", "Rajeev", "Aryan", "Rashmi", "Jaspreet", "Akbar")
        val spinner = spinner
        val arrayAdapter =
            ArrayAdapter(requireActivity(), android.R.layout.simple_spinner_item, personNames)
        spinner.adapter = arrayAdapter

        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long,
            ) {
                Toast.makeText(
                    activity,
                    getString(R.string.selected_item) + " " + personNames[position],
                    Toast.LENGTH_SHORT
                ).show()
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // Code to perform some action when nothing is selected
            }
        }
    }

    fun showDialog() {
        val fm = fragmentManager
        val newFragment = CreateNewServiceFragment()
        fm?.let { newFragment.show(it, "abc") }
    }


    private fun showDialogForQuickSell() {

        val fm = fragmentManager
        val newFragment = QuickSellFragment()
        fm?.let { newFragment.show(it, "abc") }

    }
}