package com.cubixsoft.tabletdesign.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cubixsoft.tabletdesign.R
import com.cubixsoft.tabletdesign.adapters.ArticleServiceAdapter
import com.cubixsoft.tabletdesign.adapters.LandMarksAdapter
import com.cubixsoft.tabletdesign.adapters.MultiAdapter
import com.cubixsoft.tabletdesign.adapters.QuantityAdapter
import com.cubixsoft.tabletdesign.databinding.FragmentArticleServicesBinding
import com.cubixsoft.tabletdesign.models.AprrovalElementModel
import com.cubixsoft.tabletdesign.models.ItemsViewModel
import com.google.android.material.tabs.TabLayout
import java.util.*


class ArticleAndServiceFragment : Fragment() {
    private lateinit var binding: FragmentArticleServicesBinding
    var list_of_items = arrayOf("Item 1", "Item 2", "Item 3")
    lateinit var recyclerView: RecyclerView
    private var employees: ArrayList<AprrovalElementModel> = ArrayList()
    private var adapter: MultiAdapter? = null

    companion object {

        fun newInstance(): ArticleAndServiceFragment {
            return ArticleAndServiceFragment()
        }
    }

    @SuppressLint("UseRequireInsteadOfGet", "ResourceAsColor")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        binding = FragmentArticleServicesBinding.inflate(inflater, container, false)
        recyclerView = binding.recyclerView
        binding.approval.isChecked==true
        onCicks()
        setTab()
        setArticleServicesTab()
        setupSpinnerForInProgress("Categories", binding.spCattegories)
        setupSpinnerForInProgress("Profile Varient", binding.spProfileVarient)
        setupSpinnerForInProgress("Size", binding.spSize)
        setManagmentData()
        setQuantityData()
        setlandmarksData()
        binding.llServices.setBackgroundResource(R.drawable.tranparent_shape)
        binding.llArticle.setBackgroundResource(R.drawable.gradient_shape)
        binding.llServices.setOnClickListener {
            showToast("service")
            binding.llServices.setBackgroundResource(R.drawable.gradeient_rect)
            binding.llArticle.setBackgroundResource(R.drawable.tranparent_shape)

        }
        binding.llArticle.setOnClickListener {
            binding.llServices.setBackgroundResource(R.drawable.tranparent_shape)
            binding.llArticle.setBackgroundResource(R.drawable.gradeient_rect)

        }
        return binding.root
    }

    private fun onCicks() {
        setupSpinnerForInProgress("External(Client)", binding.spRequestType)
        recyclerView.layoutManager = LinearLayoutManager(activity)
        recyclerView.addItemDecoration(DividerItemDecoration(activity,
            LinearLayoutManager.VERTICAL))
        adapter = MultiAdapter(activity, employees)
        recyclerView.adapter = adapter

        createList()
        binding.ivBack.setOnClickListener {
            binding.llnewRequestOpen.visibility = View.GONE
            binding.llRecyclerClick.visibility = View.VISIBLE
        }
        binding.llnewRequest.setOnClickListener {
            binding.llnewRequestOpen.visibility = View.VISIBLE
            binding.llRecyclerClick.visibility = View.GONE
        }


    }

    private fun createList() {
        employees = ArrayList()
        for (i in 0..4) {
            val employee = AprrovalElementModel()
            employee.setName("Oil Filter " + (i + 1))
            // for example to show at least one selection
            if (i == 0) {
                employee.setChecked(true)
            }
            //
            employees.add(employee)
        }
        adapter!!.setEmployees(employees)
    }

    private fun showToast(msg: String) {
        Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show()
    }

    private fun setTab() {
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText("Articles & Services")
        )
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText("Noded Detail")
        )
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText("Forms")
        )
        binding.tabLayout.addTab(binding.tabLayout.newTab().setText("Live & Notes")
        )

        binding.tabLayout.setOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                var fragment: Fragment? = null
                when (tab.position) {
                    //live
                    0 -> {
//                        setManagmentData()
                    }
                    //calendar
                    1 -> {

                    }
//                    request
                    2 -> {
                    }
//                    stuff
                    3,
                    -> {
//                        setStuffData()
                    }

                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {}
            override fun onTabReselected(tab: TabLayout.Tab) {}
        })

    }

    private fun setArticleServicesTab() {
        binding.tabLayout.setOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                var fragment: Fragment? = null
                when (tab.position) {
                    //live
                    0 -> {
                        binding.llNodelDetail.visibility = View.GONE
                        binding.llFormsTab.visibility = View.GONE
                        binding.llSpinnerREcycler.visibility = View.VISIBLE
                        binding.LiveANDNotes.visibility = View.GONE

                    }
                    //calendar
                    1 -> {
                        binding.llNodelDetail.visibility = View.VISIBLE
                        binding.llFormsTab.visibility = View.GONE
                        binding.llSpinnerREcycler.visibility = View.GONE
                        binding.LiveANDNotes.visibility = View.GONE

                    }
//                    forms
                    2 -> {
                        binding.llNodelDetail.visibility = View.GONE
                        binding.llFormsTab.visibility = View.VISIBLE
                        binding.llSpinnerREcycler.visibility = View.GONE
                        binding.LiveANDNotes.visibility = View.GONE

                    }
//                    stuff
                    3,
                    -> {
                        binding.llNodelDetail.visibility = View.GONE
                        binding.llFormsTab.visibility = View.GONE
                        binding.LiveANDNotes.visibility = View.VISIBLE
                        binding.llSpinnerREcycler.visibility = View.GONE
//                           binding.llNodelDetail.visibility = View.VISIBLE
//                        binding.llSpinnerREcycler.visibility = View.GONE   setStuffData()
                    }

                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {}
            override fun onTabReselected(tab: TabLayout.Tab) {}
        })

    }

    fun setManagmentData() {
        binding.rvManagemnt.layoutManager =
            GridLayoutManager(activity, 3, LinearLayoutManager.VERTICAL, false)

        // ArrayList of class ItemsViewModel
        val data = ArrayList<ItemsViewModel>()

        // This loop will create 20 Views containing
        // the image with the count of view
        data.add(ItemsViewModel(R.drawable.demo2, "Beverage "))
        data.add(ItemsViewModel(R.drawable.demo3, "Fresh Food "))

        data.add(ItemsViewModel(R.drawable.demo2, "Beverage "))
        data.add(ItemsViewModel(R.drawable.demo3, "Fresh Food "))

        data.add(ItemsViewModel(R.drawable.demo2, "Beverage "))
        data.add(ItemsViewModel(R.drawable.demo3, "Fresh Food "))

        data.add(ItemsViewModel(R.drawable.demo2, "Beverage "))
        data.add(ItemsViewModel(R.drawable.demo3, "Fresh Food "))


        // This will pass the ArrayList to our Adapter
        val adapter = ArticleServiceAdapter(requireActivity(), data)
        // Setting the Adapter with the recyclerview
        binding.rvManagemnt.adapter = adapter
    }

    fun setQuantityData() {
        binding.rv.layoutManager =
            GridLayoutManager(activity, 1, LinearLayoutManager.VERTICAL, false)

        // ArrayList of class ItemsViewModel
        val data = ArrayList<ItemsViewModel>()

        // This loop will create 20 Views containing
        // the image with the count of view
        data.add(ItemsViewModel(R.drawable.demo2, "Beverage "))
        data.add(ItemsViewModel(R.drawable.demo2, "Fresh Food "))

        data.add(ItemsViewModel(R.drawable.demo2, "Beverage "))
        data.add(ItemsViewModel(R.drawable.demo2, "Fresh Food "))


        // This will pass the ArrayList to our Adapter
        val adapter = QuantityAdapter(requireActivity(), data)
        // Setting the Adapter with the recyclerview
        binding.rv.adapter = adapter
    }

    fun setlandmarksData() {
        binding.rvLandMarks.layoutManager =
            GridLayoutManager(activity, 1, LinearLayoutManager.VERTICAL, false)

        // ArrayList of class ItemsViewModel
        val data = ArrayList<ItemsViewModel>()
        for (i in 0..2) {
            data.add(ItemsViewModel(R.drawable.demo_baba, "1. Check for Noise located Rear on Left Side, when He drives fast "))

        }

        val adapter = LandMarksAdapter(requireActivity(), data)
        // Setting the Adapter with the recyclerview
        binding.rvLandMarks.adapter = adapter
    }

    private fun setupSpinnerForInProgress(nameFirst: String, spinner: Spinner) {
        val personNames =
            arrayOf(nameFirst, "Jack", "Rajeev", "Aryan", "Rashmi", "Jaspreet", "Akbar")
        val spinner = spinner
        val arrayAdapter =
            ArrayAdapter(requireActivity(), android.R.layout.simple_spinner_item, personNames)
        spinner.adapter = arrayAdapter

        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long,
            ) {
                Toast.makeText(
                    activity,
                    getString(R.string.selected_item) + " " + personNames[position],
                    Toast.LENGTH_SHORT
                ).show()
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // Code to perform some action when nothing is selected
            }
        }
    }
}