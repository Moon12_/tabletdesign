package com.cubixsoft.tabletdesign.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cubixsoft.tabletdesign.R
import com.cubixsoft.tabletdesign.adapters.ManagmentAdapter
import com.cubixsoft.tabletdesign.adapters.ParentAdapter
import com.cubixsoft.tabletdesign.databinding.FragmentHomeBinding
import com.cubixsoft.tabletdesign.models.ItemsViewModel
import com.cubixsoft.tabletdesign.models.ParentDataFactory
import java.util.*

class HomeFragment : Fragment() {
    private lateinit var binding: FragmentHomeBinding
    var list_of_items = arrayOf("Item 1", "Item 2", "Item 3")
    lateinit var recyclerView: RecyclerView

    companion object {

        fun newInstance(): HomeFragment {
            return HomeFragment()
        }
    }

    @SuppressLint("UseRequireInsteadOfGet", "ResourceAsColor")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        binding.setting.setOnClickListener {
            activity!!.supportFragmentManager
                // 3
                .beginTransaction()
                // 4
                .add(R.id.fragment, ArticleAndServiceFragment.newInstance(), "dogList")
                // 5
                .commit()

        }
        initRecycler()
        onClicks()
        return binding.root
    }




    private fun onClicks() {
        binding.createService.setOnClickListener {
            showDialog()

        }

        binding.llQuickSell.setOnClickListener {
//            Toast.makeText(activity,"clicked",Toast.LENGTH_SHORT).show()
            val fm = fragmentManager
            val newFragment = QuickSellFragment()
            fm?.let { newFragment.show(it, "abc") }

        }
    }

    @SuppressLint("WrongConstant")
    private fun initRecycler() {
        recyclerView = binding.rvManagemnt

        recyclerView.apply {
            layoutManager = LinearLayoutManager(activity, LinearLayout.VERTICAL, false)
            adapter = ParentAdapter(ParentDataFactory.getParents(20))
        }

    }
    fun showDialog() {
        val fm = fragmentManager
        val newFragment = CreateNewServiceFragment()
        fm?.let { newFragment.show(it, "abc") }
    }
    fun setManagmentData() {
        binding.rvManagemnt.layoutManager =
            GridLayoutManager(activity, 1, LinearLayoutManager.VERTICAL, false)

        // ArrayList of class ItemsViewModel
        val data = ArrayList<ItemsViewModel>()

        // This loop will create 20 Views containing
        // the image with the count of view
        data.add(ItemsViewModel(R.drawable.menu_primary, "Beverage "))
        data.add(ItemsViewModel(R.drawable.menu_primary, "Fresh Food "))
        data.add(ItemsViewModel(R.drawable.menu_primary, "Beverage "))
        data.add(ItemsViewModel(R.drawable.menu_primary, "Fresh Food "))
        data.add(ItemsViewModel(R.drawable.menu_primary, "Beverage "))
        data.add(ItemsViewModel(R.drawable.menu_primary, "Fresh Food "))
        data.add(ItemsViewModel(R.drawable.menu_primary, "Beverage "))
        data.add(ItemsViewModel(R.drawable.menu_primary, "Fresh Food "))

        // This will pass the ArrayList to our Adapter
        val adapter = ManagmentAdapter(requireActivity(), data)
        // Setting the Adapter with the recyclerview
        binding.rvManagemnt.adapter = adapter
    }


}